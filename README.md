Instructions to set up GrayLog here:
http://docs.graylog.org/en/2.1/pages/installation/docker.html

Application needs to be added as a docker container to work (Input node endpoint is within the container network)

Logback logging details here:
https://marketplace.graylog.org/addons/a3c8bbdf-6b47-43fe-8298-f259db85fc59

