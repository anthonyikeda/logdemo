package au.org.ikeda.logging;

import org.slf4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
public class LogDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(LogDemoApplication.class, args);
	}
}

@RestController
class MainController {

	private Logger log = org.slf4j.LoggerFactory.getLogger(MainController.class);

	@RequestMapping(value="/")
	public String message() {
		log.info("This is a message");
		return "Hello";
	}
}